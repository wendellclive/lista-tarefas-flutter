import "package:flutter/material.dart";
import "package:path_provider/path_provider.dart";
import "dart:io";
import "dart:convert";

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  List _listaTarefas = [];
  TextEditingController _editingController = TextEditingController();

  _salvarTarefa() {
    String textoDigitado = _editingController.text;

    Map<String, dynamic> tarefa = Map();
    tarefa["titulo"] = textoDigitado;
    tarefa["realizada"] = false;

    setState(() {
      _listaTarefas.add((tarefa));
    });

    _salvarArquivo();
    _editingController.text = "";
  }

  _salvarArquivo() async {
    var arquivo = await _getFile();

    String dados = json.encode(_listaTarefas);
    arquivo.writeAsString(dados);
  }

  _getFile() async {
    final diretorio = await getApplicationDocumentsDirectory();
    return File("${diretorio.path}/dados.json");
  }

  _lerArquivo() async {
    try {
      final arquivo = await _getFile();
      return arquivo.readAsString();
    } catch (e) {
      return null;
    }
  }

  @override
  void initState() {
    super.initState();
    _lerArquivo().then((dados) {
      setState(() {
        _listaTarefas = jsonDecode(dados);
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Lista de Tarefas",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.purple,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.purple,
          onPressed: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text("Adicionar Tarefa"),
                    content: TextField(
                      controller: _editingController,
                      decoration:
                      InputDecoration(labelText: "Digite sua tarefa: "),
                      onChanged: (text) {},
                    ),
                    actions: [
                      TextButton(
                          child: Text("Cancelar"),
                          onPressed: () => Navigator.pop(context)),
                      TextButton(
                        child: Text("Salvar"),
                        onPressed: () {
                          _salvarTarefa();
                          Navigator.pop(context);
                        },
                      )
                    ],
                  );
                });
          }),
      body: Column(
        children: [
          Expanded(
              child: ListView.builder(
                  itemCount: _listaTarefas.length,
                  itemBuilder: (context, index) {
                    return CheckboxListTile(
                        title: Text(_listaTarefas[index]['titulo']),
                        value: _listaTarefas[index]['realizada'],
                        onChanged: (valorAlterado) {
                          setState(() {
                            _listaTarefas[index]['realizada'] = valorAlterado;
                          });
                          _salvarArquivo();
                        });
                  })
          )],
      ),
    );
  }
}
